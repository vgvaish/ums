<?php
require("header.php");

$search = $_REQUEST["search"];
$sql = "SELECT * FROM vg_users WHERE username LIKE '%$search%'";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$records = $stmt->fetchAll();
?>

<html>
	<head>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">

                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"crossorigin="anonymous"></script>

        </head>
	<body>
		<table class="table table-striped">
			<thead>
				<th>Id</th>
				<th>User Name</th>
				<th>password</th>
				<th>Name</th>
				<th>Age</th>
				<th>City</th>
				<th>Updated_date</th>
				<th>Added_date</th>
				<th>satus</th>
			</thead>
			<tbody>

				<?php foreach($records as $row) { ?>

					<tr>

						<td><?php echo $row['id']; ?></td>
						<td><?php echo $row['username']; ?></td>
						<td><?php echo $row['password']; ?></td>
						<td><?php echo $row['name']; ?></td>
						<td><?php echo $row['age']; ?></td>
						<td><?php echo $row['city']; ?></td>
						<td><?php echo $row['updated_date']; ?></td>
						<td><?php echo $row['added_date']; ?></td>
						<td><?php echo $row['status']; ?></td>
					</tr>

				<?php } ?>

			</tbody>
		</table>
	</body>
</html>
