<?php

require("header.php");

//authorization();


?>
<head>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body class="d-flex flex-column h-100 container" style="background-image: url('https://www.google.com/url?sa=i&url=https%3A%2F%2Fwallpaperaccess.com%2Femployee&psig=AOvVaw1yhyPBfoBHKeDyaadM_bO5&ust=1634191278438000&source=images&cd=vfe&ved=0CAgQjRxqFwoTCIiTqY3bxvMCFQAAAAAdAAAAABAE');
    background-repeat: no-repeat; background-size: cover;">

	<h3 style="margin-top: 10px; font-style: italic; font-weight: bold;text-align: center;">Add Record</h3>
        <center>
		<form action="add_form_action.php" method="POST" style="margin-top: 20px; background-color: rgba(0, 0, 0, 0.6); padding: 40px;color: white; border-radius: 10px;max-width: 800px; text-align: left;" >
			<div class="mb-3">
                		<label for="name" class="form-label">User Name</label>
				<div><input type="text" name="username"  class="w-100 p-1"/></div>
			</div>
			<div class="mb-3">
                	        <label for="name" class="form-label">password</label>
				<div><input type="password" name="password"  class="w-100 p-1"/></div>
			</div>
			<div class="mb-3">
                	        <label for="name" class="form-label">Name</label>
				<div><input type="text" name="name"  class="w-100 p-1"/></div>
			</div>
			 <div class="mb-3">
                       		<label for="name" class="form-label">Age</label>
				<div><input type="text" name="age"  class="w-100 p-1"/></div>
			</div>
			<div class="mb-3">
                	        <label for="name" class="form-label">City</label>
				<select name="city">
					<option value="Mumbai">Mumbai</option>
					<option value="Hyderabad">Hyderabad</option>
					<option value="Bangalore">Bangalore</option>
					<option value="Nagpur">Nagpur</option>
					<option value="Pune">Pune</option>
				</select>
			</div>

			<input type="submit" class="btn btn-success" value="Add"/>&nbsp;&nbsp;
			<input type="reset" class="btn btn-dark" value="Clear"/>&nbsp;&nbsp;
			<a href="dashboard.php" class="btn btn-warning" >Close</a>

</form>
</center>
</body>
