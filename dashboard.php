<?php
require('header.php');
?>

<html>
	<center>
		<head>
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">

                	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"crossorigin="anonymous"></script>
		</head>
		<header>
			<h2 style="margin-top: 10px; font-style: font-weight: bold;text-align: center;">User Management System</h2>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container-fluid">
					<a class="btn btn-primary" role="button" href="add_form.php">ADD Record</a>
					<form class="d-flex" action="search.php">
						<input class="form-control me-2" type="search" name="search" placeholder="Search" aria-label="Search">
						<button class="btn btn-outline-success" type="submit">Search</button>
					</form>
				</div>
			</nav>
		</header>
		<body>
			<table class="table table-striped table-hover text-center" >

                        	<thead class="table table-dark">
					<th> Id </th>
					<th> USER Name </th>
					<th> Password </th>
					<th> Added Date </th>
					<th> UpDate Date </th>
                               		<th> Name </th>
                               		<th> Age </th>
					<th> City </th>
					<th> Status </th>
					<th> SEE Details </th>
					<th> Action </th>
				</thead>
                        <tbody>

                                <?php foreach($records as $row) { ?>

					<tr>
						<td><?php echo $row['id']; ?></td>
						<td><?php echo $row['username']; ?></td>
                                                <td><?php echo $row['password']; ?></td>
                                                <td><?php echo $row['name']; ?></td>
                                                <td><?php echo $row['age']; ?></td>
						<td><?php echo $row['city']; ?></td>
						<td><?php echo $row['added_date']; ?></td>
                                                <td><?php echo $row['updated_date']; ?></td>
						<td><?php echo $row['status']; ?></td>

                                                <td><button onclick="showSingleUser(<?php echo $row["id"]; ?>)" class="btn" data-bs-toggle="modal" data-bs-target="#exampleModal"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
  <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
  <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
</svg></button></td>
						<td><a href="edit_form.php?id=<?php echo $row["id"]; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
</svg></a>
							&nbsp;| &nbsp;
							<a href="delete.php?id=<?php echo $row["id"]; ?>" onclick="return confirm('Are you sure you want to delete this record?');"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
  <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
</svg></a>
                                                </td>

                                        </tr>

                                <?php } ?>

                        </tbody>
                </table>
	<footer>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container-fluid nav justify-content-center">
				<a href="login_form.php" class="btn btn-primary" >Logout</a>
			</div>
		</nav>
	</footer>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">User Details</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
                User Name : <span id="username"></span><br>
                Name : <span id="name"></span><br>
                Age : <span id="age"></span><br>
                Address : <span id="city"></span><br>
                status : <span id="status"></span><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</body>
	<script>

function showSingleUser(id){
	url = `getdata_action.php?id=${id}`
        fetch(url)
        .then(data=>data.json())
        .then(data=> {
        document.getElementById('username').innerHTML = data['username'];
        document.getElementById('name').innerHTML = data['name'];
        document.getElementById('age').innerHTML = data['age'];
	document.getElementById('city').innerHTML = data['city'];
	document.getElementById('status').innerHTML = data['status'];
}
      );
}
		</script>
	</center>
</html>
