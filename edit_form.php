<?php

require("function.php");

$conn= getConn('localhost', 'diituser', '%TGBbgt5', 'diit');
$sql = "SELECT * FROM vg_users WHERE id = '$_REQUEST[id]'";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$record = $stmt->fetch();

?>

<head>
        <title>EDIT USER</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
</head>
        <body class="d-flex flex-column h-100 container" style="background-image: url('https://www.google.com/url?sa=i&url=https%3A%2F%2Fwallpaperaccess.com%2Femployee&psig=AOvVaw1yhyPBfoBHKeDyaadM_bO5&ust=1634191278438000&source=images&cd=vfe&ved=0CAgQjRxqFwoTCIiTqY3bxvMCFQAAAAAdAAAAABAE');
    background-repeat: no-repeat; background-size: cover;">
                <header>
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                          <div class="container-fluid">
                                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                                  </div>
                                </div>
                          </div>
                        </nav>
                </header>

                <h3 style="margin-top: 10px; font-style: italic; font-weight: bold;text-align: center;">Edit Record</h3>
                <center>

<form action="edit_action.php" method="POST" style="margin-top: 20px; background-color: rgba(0, 0, 0, 0.6); padding: 20px;color: white; border-radius: 10px;max-width: 800px; text-align: left;">
	<input type="hidden" name="id" value="<?php echo $record["id"] ?>"/>
	<div class="mb-3">
		<label for="name" class="form-label">Name</label>
		<div><input type="text" name="name"  class="w-100 p-1" value="<?php echo $record["name"]; ?>"/></div>
	</div>
	<div class="mb-3">
		<label for="contact" class="form-label">AGE</label>	
		<div><input type="text" name="age"  class="w-100 p-1" value="<?php echo $record["age"]; ?>"/></div>
	</div>
	<div class="mb-3">
                <label for="contact" class="form-label">City</label>
		<div><select name="city">
				<option value="Mumbai" <?php echo trim($record["city"]) == 'Mumbai' ? 'selected' : ''; ?>>Mumbai</option>
				<option value="Hyderabad" <?php echo trim($record["city"]) == 'Hyderabad' ? 'selected' : ''; ?>>Hyderabad</option>
				<option value="Banglore" <?php echo trim($record["city"]) == 'Mumbai' ? 'selected' : ''; ?>>Banglore</option>
				<option value="Nagpur" <?php echo trim($record["city"]) == 'Nagpur' ? 'selected' : ''; ?>>Nagpur</option>
				<option value="Pune" <?php echo trim($record["city"]) == 'Pune' ? 'selected' : ''; ?>>Pune</option>
		</select></div>
	</div>
	<div class="mb-3">
                <label for="contact" class="form-label">STATUS</label>
		<div><select name="status">
                                <option value="active" <?php echo $record["status"] == 'active' ? 'selected' : ''; ?> >Active</option>
                                <option value="inactive" <?php echo $record["status"] == 'inactive' ? 'selected' : ''; ?> >Inactive</option>
		</select></div>
	</div>
	<center>
		<input type="submit" value="Update" class="btn btn-success"/>&nbsp;&nbsp;
		<input type="reset" value="Clear" class="btn btn-dark"/>&nbsp;&nbsp;
		<a href="dashboard.php" class="btn btn-warning">Close</a>
	</center>
</form>

